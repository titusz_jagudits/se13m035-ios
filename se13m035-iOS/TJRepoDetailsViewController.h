//
//  TJRepoDetailsViewController.h
//  se13m035-iOS
//
//  Created by se13m035 on 19/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import <UIKit/UIKit.h>

#define REPO_DETAILS_TITLE_PREFIX @"Details of the repo "

@interface TJRepoDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *repoNameUILabel;
@property (weak, nonatomic) IBOutlet UILabel *repoFullNameUILabel;
@property (weak, nonatomic) IBOutlet UILabel *repoDescriptionUILabel;
@property (weak, nonatomic) IBOutlet UILabel *repoHomepageUILabel;

@property (strong, nonatomic) NSString *repoName;
@property (strong, nonatomic) NSString *loginName;

-(void)setTextForLabel:(UILabel *)label responseObject:(id)responseObject forKey:(NSString *)key;

@end
