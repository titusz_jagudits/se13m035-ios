//
//  AuthorizeViewController.m
//  se13m035-iOS
//
//  Created by se13m035 on 16/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import "AuthorizeViewController.h"
#import "AppDelegate.h"

@interface AuthorizeViewController ()

@end

@implementation AuthorizeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)authorize:(id)sender {
    
    NSURL *authorizeUrl = [NSURL URLWithString:
                           [@"https://github.com/login/oauth/authorize?client_id=" stringByAppendingString:CLIENT_ID]];
    [[UIApplication sharedApplication] openURL:authorizeUrl];
    
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
