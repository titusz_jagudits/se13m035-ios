//
//  AppDelegate.h
//  se13m035-iOS
//
//  Created by se13m035 on 14/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <AFNetworking/AFNetworking.h>

// GitHub OAuth app account
#define CLIENT_ID @"7a81a100c885c08cd063"
#define CLIENT_SECRET @"6ed0c2b60ecbdf9bf89e68917ef874cda88d05a7"
#define CLIENT_CALLBACK_URL @"tjgithubclient-ios://authorize"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (strong, nonatomic) NSString *accessToken;

@end

