//
//  TJRepoListTableViewController.h
//  se13m035-iOS
//
//  Created by se13m035 on 18/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import <UIKit/UIKit.h>

#define REPO_LIST_TITLE_PREFIX @"Repositories of user "

@interface TJRepoListTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *repos;

@property (strong, nonatomic) NSString *loginName;

@end
