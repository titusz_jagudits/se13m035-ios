//
//  AuthorizeViewController.h
//  se13m035-iOS
//
//  Created by se13m035 on 16/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthorizeViewController : UIViewController

- (IBAction)authorize:(id)sender;

@end
