//
//  TJRepoDetailsViewController.m
//  se13m035-iOS
//
//  Created by se13m035 on 19/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import "TJRepoDetailsViewController.h"
#import "AppDelegate.h"

@interface TJRepoDetailsViewController ()

@end

@implementation TJRepoDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = [REPO_DETAILS_TITLE_PREFIX stringByAppendingString:self.repoName];

    [self loadRepoDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)loadRepoDetails
{
    NSLog(@"loadRepoDetails of repo: %@", self.repoName);
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *parameters = @{@"access_token": delegate.accessToken};

//    /repos/octokit/octokit.rb
//    /user/fua
    NSString *repoUrl = [NSString stringWithFormat:@"https://api.github.com/repos/%@/%@",
                         self.loginName,
                         self.repoName];
    [manager GET:repoUrl
      parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"loadRepoDetails JSON: %@", responseObject);

//             NSLog(@"loadRepoDetails name: %@", [responseObject valueForKey:@"name"]);
//             self.repoNameUILabel.text = [responseObject valueForKey:@"name"];
             [self setTextForLabel:self.repoNameUILabel responseObject:responseObject forKey:@"name"];

//             NSLog(@"loadRepoDetails full_name: %@", [responseObject valueForKey:@"full_name"]);
//             self.repoFullNameUILabel.text = [responseObject valueForKey:@"full_name"];
             [self setTextForLabel:self.repoFullNameUILabel responseObject:responseObject forKey:@"full_name"];

//             NSLog(@"loadRepoDetails description: %@", [responseObject valueForKey:@"description"]);
//             self.repoDescriptionUILabel.text = [responseObject valueForKey:@"description"];
             [self setTextForLabel:self.repoDescriptionUILabel responseObject:responseObject forKey:@"description"];

//             NSLog(@"loadRepoDetails homepage: %@", [responseObject valueForKey:@"homepage"]);
//             self.repoHomepageUILabel.text = [responseObject valueForKey:@"homepage"];
             [self setTextForLabel:self.repoHomepageUILabel responseObject:responseObject forKey:@"homepage"];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"loadRepoDetails Error: %@", error);
         }];
}

-(void)setTextForLabel:(UILabel *)label responseObject:(id)responseObject forKey:(NSString *)key
{
    NSLog(@"setTextForLabel label:%@, key:%@, responseObject:%@", label, key, responseObject);

    if (responseObject != nil && [responseObject valueForKey:key] != [NSNull null]) {
        label.text = [responseObject valueForKey:key];
    } else {
        label.text = @"n/a";
    }
}
@end
