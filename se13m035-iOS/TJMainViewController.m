//
//  TJMainViewController.m
//  se13m035-iOS
//
//  Created by se13m035 on 16/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import "TJMainViewController.h"
#import "AppDelegate.h"
#import "TJRepoListTableViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface TJMainViewController ()

@end

@implementation TJMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    if (delegate.accessToken == nil) {
        //        [self presentViewController:<#(UIViewController *)#> animated:YES completion:<#^(void)completion#>];
        [self performSegueWithIdentifier:@"authNeededSegue" sender:self];
    } else {
        //NSLog(@"viewDidLoad userProfileNameLabel.text: %@", self.userProfileNameLabel.text);
        //NSLog(@"viewDidLoad userProfileNameLabel.text length: %lu", (unsigned long)[self.userProfileNameLabel.text length]);
        if ([self.userProfileNameLabel.text length] == 0) {
            [self loadUserProfile];
        }
    }
    
    [delegate addObserver:self forKeyPath:@"accessToken" options:NSKeyValueObservingOptionNew context:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadUserProfile {
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *parameters = @{@"access_token": delegate.accessToken};
    
    [manager GET:@"https://api.github.com/user"
      parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
             
             self.userProfileNameLabel.text = [responseObject valueForKey:@"name"];

             self.userProfileLoginLabel.text = [responseObject valueForKey:@"login"];
             self.userProfileEmailLabel.text = [responseObject valueForKey:@"email"];
             self.userProfileCompanyLabel.text = [responseObject valueForKey:@"company"];

             NSString *avatarUrlString = [responseObject valueForKey:@"avatar_url"];
             NSURL *avatarURL = [NSURL URLWithString:avatarUrlString];
             if (avatarURL) {
                 [self.userProfileImageView setImageWithURL:avatarURL];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"accessToken"]) {
        NSLog(@"observeValueForKeyPath accessToken has changed, %@", object);
        [self loadUserProfile];
    }
    
    //    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if([segue.identifier isEqualToString:@"segueToTJRepoList"])
    {
        UINavigationController *navCtrl = segue.destinationViewController;
        TJRepoListTableViewController *repoListCtrl = (TJRepoListTableViewController*)navCtrl.topViewController;

        repoListCtrl.loginName = self.userProfileLoginLabel.text;
    }
}
@end
