//
//  TJRepoListTableViewController.m
//  se13m035-iOS
//
//  Created by se13m035 on 18/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import "TJRepoListTableViewController.h"
#import "AppDelegate.h"
#import "TJMainViewController.h"
#import "TJRepoDetailsViewController.h"


@interface TJRepoListTableViewController ()

@end

@implementation TJRepoListTableViewController

- (void)loadRepos
{
    NSLog(@"loadRepos repos at start: %@", self.repos);
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *parameters = @{@"access_token": delegate.accessToken,
                                 //@"sort": @"created"
                                 @"type": @"owner"
                                 };
    
    [manager GET:@"https://api.github.com/user/repos"
      parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"loadRepos JSON: %@", responseObject);
             //NSLog(@"loadRepos class: %@", [[responseObject valueForKey:@"name"] class]);
             //NSLog(@"loadRepos class2: %@", [self.repos class]);
             
             NSArray *reposArray = [responseObject valueForKey:@"name"];
             //             NSString *repoName;
             NSString *repoName;
             for (repoName in reposArray) {
                 //NSLog(@"loadRepos repoName class: %@", [repoName class]);
                 //NSLog(@"loadRepos what class: %@", [@"a string" class]);
                 [self.repos addObject:[repoName stringByStandardizingPath]];
             }
             
             //id proxy = [self mutableArrayValueForKey:@"repos"];
             //[proxy insertObject:@"00000" atIndex:0];
             //NSLog(@"loadRepos proxy: %@", proxy);
             
             //[self.repos arrayByAddingObjectsFromArray:reposArray];
             NSLog(@"loadRepos repos: %@", self.repos);
             NSLog(@"loadRepos repos count: %lu", (unsigned long)[self.repos count]);
             
             [self.tableView reloadData];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"loadRepos Error: %@", error);
         }];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = [REPO_LIST_TITLE_PREFIX stringByAppendingString:self.loginName];
    
    self.repos = [[NSMutableArray alloc] init];
    
    // some init data for testing
    //[self.repos addObject:@"repo1"];
    //[self.repos addObject:@"repo22"];
    //[self.repos addObject:@"repo333"];
    //NSLog(@"viewDidLoad repos count: %lu", (unsigned long)[self.repos count]);
    
    //    [self addObserver:self forKeyPath:@"repos" options:NSKeyValueObservingOptionNew context:nil];
    
    [self loadRepos];
}

- (void)viewWillAppear:(BOOL)animated {
    //    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    //    [self addObserver:self forKeyPath:@"repos" options:NSKeyValueObservingOptionNew context:nil];
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    NSLog(@"observeValueForKeyPath keyPath: %@", keyPath);
//
//    if ([keyPath isEqualToString:@"repos"]) {
//        //NSLog(@"observeValueForKeyPath accessToken has changed, %@", object);
//        [self.tableView reloadData];
//    }
//
//
//    //[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    //return 0;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    //return 0;
    NSLog(@"numberOfRowsInSection repos count: %lu", (unsigned long)[self.repos count]);
    return [self.repos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListPrototypeCell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSLog(@"cellForRowAtIndexPath indexPath.row: %ld", (long)indexPath.row);
    NSLog(@"cellForRowAtIndexPath objectAtIndex %@", [self.repos objectAtIndex:indexPath.row]);
    cell.textLabel.text = [self.repos objectAtIndex:indexPath.row];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue segue.identifier: %@", segue.identifier);
    
    if([segue.identifier isEqualToString:@"segueToTJRepoDetails"])
    {
        UINavigationController *navCtrl = segue.destinationViewController;
        TJRepoDetailsViewController *repoDetailsCtrl = (TJRepoDetailsViewController*)navCtrl.topViewController;

        UITableViewCell *cell = sender;
        NSLog(@"prepareForSegue cell.text: %@", cell.textLabel.text);
        repoDetailsCtrl.repoName = cell.textLabel.text;
        repoDetailsCtrl.loginName = self.loginName;
    }
}

@end
