//
//  TJMainViewController.h
//  se13m035-iOS
//
//  Created by se13m035 on 16/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TJMainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userProfileNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userProfileLoginLabel;
@property (weak, nonatomic) IBOutlet UILabel *userProfileEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *userProfileCompanyLabel;

//- (IBAction)authorize:(id)sender;
- (void)loadUserProfile;

@end