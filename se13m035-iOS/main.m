//
//  main.m
//  se13m035-iOS
//
//  Created by se13m035 on 14/01/15.
//  Copyright (c) 2015 se13m035. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
